module.exports = function (grunt) {
    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        connect: {
            server: {
                options: {
                    port: 8888,
                    base: 'build/'
                }
            }
        },
        copy: {
            html: {
                flatten: true,
                files: [

      // includes files within path
                    {
                        cwd: 'src', // set working folder / root to copy
                        expand: true,
                        src: ['index.html'], //For now index is working
                        dest: 'build/'
                    },
                    {
                        cwd: 'src', // set working folder / root to copy
                        expand: true,
                        src: ['fonts/*'], //For now index is working
                        dest: 'build/'
                    }
                        ],
            },
            fonts: {
                flatten: true,
                files: [

      // includes files within path

                    {
                        cwd: 'src', // set working folder / root to copy
                        expand: true,
                        src: ['fonts/*'], //For now index is working
                        dest: 'build/'
                    }
                        ],

            }
        },
        htmlmin: { // Task
            dist: { // Target
                options: { // Target options
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: { // Dictionary of files
                    'production/index.html': 'build/index.html' // 'destination': 'source'

                }
            }
        },


        sass: {
            dist: {
                options: {
                    style: 'uncompressed'
                },
                files: {
                    'build/css/style.css': 'src/scss/style.scss'
                }
            }
        },

        jshint: {
            all: ['src/js/**/*.js'],
            vendor: ['src/js/vendor/**/*.js'],
            myScripts: ['src/js/scripts/*.js']
        },

        concat: {
            vendor: {
                src: [
            'src/js/vendor/modernizr/modernizr.js', 'src/js/vendor/jquery/jquery.js', 'src/js/vendor/analytics/analytics.js', 'src/js/vendor/scrollmagic/ScrollMagic.js', 'src/js/vendor/foundation/foundation.js', 'src/js/vendor/greensock/**/*.js', 'src/js/vendor/readmore/readmore.js' // All JS in src/js folder
           // 'js/global.js' // Specific (for future purpose
        ],
                dest: 'build/js/vendor/vendor.js',
            },
            preload: {
                src: [
            'src/js/vendor/preload/**/*.js' // All JS in src/js folder
           // 'js/global.js' // Specific (for future purpose
        ],
                dest: 'build/js/vendor/preload.js',
            },
            plugins: {
                src: [
            'src/js/vendor/plugins/**/*.js' // All JS in src/js folder
           // 'js/global.js' // Specific (for future purpose
        ],
                dest: 'build/js/vendor/plugins.js',
            },
            myScripts: {
                src: [
            'src/js/scripts/**/*.js', // All JS in src/js folder
           // 'js/global.js' // Specific (for future purpose
        ],
                dest: 'build/js/scripts/scripts.js',
            },
            debug: {
                src: [
            'src/js/vendor/debug/*.js', // All JS in src/js folder
           // 'js/global.js' // Specific (for future purpose
        ],
                dest: 'build/js/temp/debug.js',
            }


        },
        uglify: {
            vendor: {
                src: 'build/js/vendor/vendor.js',
                dest: 'production/js/vendor/vendor.js'
            },
            plugins: {
                src: 'build/js/vendor/plugins.js',
                dest: 'production/js/vendor/plugins.js'
            },
            myScripts: {
                src: 'build/js/scripts/scripts.js',
                dest: 'production/js/scripts/scripts.js'
            }

        },
        cssmin: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            build: {
                files: {
                    'production/css/style.css': 'build/css/style.css'
                }
            }
        },
        postcss: {
            options: {
                map: true, // inline sourcemaps

                // or


                processors: [
        require('autoprefixer')({
                        browsers: 'last 3 version, > 15%'
                    }), // add vendor prefixes
      ]
            },
            task: {
                src: 'build/css/style.css'
            }
        },

        imagemin: {
            build: {
                files: [{
                    expand: true,
                    cwd: 'src/img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'build/img'
        }]
            },
            production: {
                files: [{
                    expand: true,
                    cwd: 'src/img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'production/img'
        }]
            }
        },
        watch: {
            options: {
                livereload: true
            },
            html: {
                files: ['src/index.html'],
                tasks: ['copy:html'],
                options: {}
            },
            scripts: {
                files: ['src/js/**/*.js'],
                tasks: ['jshint:myScripts', 'concat'],
                options: {}
            },
            sass: {
                files: ['src/scss/**/*'],
                tasks: ['sass', 'postcss'],
                options: {}
            },
            images: {
                files: ['src/img/**/*'],
                tasks: ['imagemin:build'],
                options: {}
            },
            fonts: {
                files: ['src/fonts/**/*'],
                tasks: ['copy:fonts'],
                options: {}
            }
        }

    });

    // 3. Get Grunt plugins

    //Html
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    //Sass
    grunt.loadNpmTasks('grunt-contrib-sass');
    //Compiled Css
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    //Autoprefixer
    grunt.loadNpmTasks('autoprefixer');
    grunt.loadNpmTasks('grunt-autoprefixer');
    //PostCSS
    grunt.loadNpmTasks('grunt-postcss');
    //Javascripts
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    //Jshint
    grunt.loadNpmTasks('grunt-contrib-jshint');
    //Images
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    //Reload
    grunt.loadNpmTasks('grunt-contrib-connect');
    //Watcher
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Fire grunt

    /*Default Task*/
    grunt.registerTask('default', ['jshint']);

    /*Build Task*/
    grunt.registerTask('build', ['connect', 'jshint:myScripts', 'copy', 'sass', 'postcss', 'imagemin:build', 'concat', 'watch']);
    /*Production Task*/
    grunt.registerTask('final', ['copy', 'htmlmin', 'sass', 'postcss', 'cssmin', 'concat', 'uglify', 'imagemin:production']);


};
