$(document).foundation({
    orbit: {
        animation: 'fade', // Sets the type of animation used for transitioning between slides, can also be 'fade'
        timer_speed: 4000, // Sets the amount of time in milliseconds before transitioning a slide
        pause_on_hover: false, // Pauses on the current slide while hovering
        resume_on_mouseout: false, // If pause on hover is set to true, this setting resumes playback after mousing out of slide
        animation_speed: 500, // Sets the amount of time in milliseconds the transition between slides will last
        stack_on_small: false,
        navigation_arrows: false,
        slide_number: false,
        bullets: false, // Does the slider have bullets visible?
        circular: true, // Does the slider should go to the first slide after showing the last?
        timer: true, // Does the slider have a timer active? Setting to false disables the timer.
        variable_height: false, // Does the slider have variable height content?
        swipe: true,
        timer_container_class: 'orbit-timer',
    }
});

console.info("Hi!:), Thanks for looking in my code, if you see something that could be build better, plz contact me.");


$('article').readmore({
    speed: 750,
    collapsedHeight: 0,
    lessLink: '<a href="#" class="readMore">Sluiten</a>',
    moreLink: '<a href="#" class="readMore">Lees meer</a>'
});