$(document).ready(function(){
  console.log("ready");


if (window.matchMedia("(min-width: 1024px)").matches) {
/* the view port is at least 768 pixels wide */

    // init controller
	var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

	// build scenes
	new ScrollMagic.Scene({triggerElement: ".parallax-parent-first"})
					.setTween(".parallax-parent-first > div", {y: "80%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);

	new ScrollMagic.Scene({triggerElement: ".parallax2"})
					.setTween(".parallax2 > div", {y: "80%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);

	new ScrollMagic.Scene({triggerElement: ".parallax3"})
					.setTween(".parallax3 > div", {y: "80%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);

	new ScrollMagic.Scene({triggerElement: ".parallax4"})
					.setTween(".parallax4 > div", {y: "80%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);

	/*new ScrollMagic.Scene({triggerElement: "#parallax5"})
					.setTween("#parallax5 > div", {y: "20%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);*/


}
    });
