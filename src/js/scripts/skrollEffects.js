$(document).ready(function() {
  console.log("ready");


  if (window.matchMedia("(min-width: 1024px)").matches) {
    /* the view port is at least 1024px wide */
    var controller = new ScrollMagic.Controller();

    new ScrollMagic.Scene({
        triggerElement: ".parallax-parent-first",
        duration: 550
      })
      .setClassToggle(".top-bar-section .menuItem:nth-child(1)", "active") // add class toggle
      .addIndicators("mafklapper") // add indicators (requires plugin)
      .addTo(controller);


    new ScrollMagic.Scene({
        triggerElement: ".section-second",
        duration: 600
      })
      .setClassToggle(".top-bar-section .menuItem:nth-child(2)", "active")
      .addIndicators() // add indicators (requires plugin)
      .addTo(controller);


    new ScrollMagic.Scene({
        triggerElement: ".section-third",
        duration: 600
      })
      .setClassToggle(".top-bar-section .menuItem:nth-child(3)", "active")
      .addIndicators() // add indicators (requires plugin)
      .addTo(controller);

    new ScrollMagic.Scene({
        triggerElement: ".section-fourth",
        duration: 800
      })
      .setClassToggle(".top-bar-section .menuItem:nth-child(4)", "active")
      .addIndicators() // add indicators (requires plugin)
      .addTo(controller);


    new ScrollMagic.Scene({
        triggerElement: ".section-sixth",
        duration: 600
      })
      .setClassToggle(".top-bar-section .menuItem:nth-child(5)", "active")
      .addIndicators() // add indicators (requires plugin)
      .addTo(controller);

  }

});
